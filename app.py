import os
import sys
import settings
import logging
import time
import requests

from datetime import datetime

from flask import Flask
from flask import (
    render_template,
    jsonify,
    make_response,
    send_from_directory,
    abort,
    g,
    request,
    redirect
)
from gevent.pywsgi import WSGIServer

from handlers.helpers import webpack
from handlers.portal import session

import handlers.profile as profilelib
import handlers.repository as repolib


logger = logging.getLogger('gitbucket')


def add_routes(app):

    @app.before_request
    def before_request():
        pass


    @app.errorhandler(404)
    def page_not_found(error):
        return render_template('error_4xx.html')


    @app.route('/')
    def default():
        return redirect('/rakeshgunduka')


    @app.route('/<string:username>')
    def profile(username):
        profile = profilelib.get(
            username=username, include_repos=True
        )
        if profile:
            return render_template('profile/profile.html', data=profile)
        abort(404)
        

    @app.route('/<string:username>/<string:repo>')
    @app.route('/<string:username>/<string:repo>/<string:_type>/<string:branch>')
    @app.route('/<string:username>/<string:repo>/<string:_type>/<string:branch>/<path:path>')
    def repo(username, repo, _type='tree', branch='master', path=''):
        repo_info = repolib.get(
            username=username, repo=repo, _type=_type,
            branch=branch, path=path
        )
        if repo_info:
            is_root_url = request.path == f'/{username}/{repo}'
            return render_template('repo/repo.html',
                data=repo_info, path=path, is_root_url=is_root_url
            )
        abort(404)


    @app.route('/<string:username>/<string:repo>/commit/<string:sha>')
    def commit(username, repo, sha):
        abort(404)


    @app.route('/api/<string:username>')
    def api_profile(username):
        profile = profilelib.get(username=username)
        return jsonify(profile)


    @app.route('/api/<string:username>/<string:repo>')
    @app.route('/api/<string:username>/<string:repo>/<string:_type>/<string:branch>')
    @app.route('/api/<string:username>/<string:repo>/<string:_type>/<string:branch>/<path:path>')
    def api_repo(username, repo, _type='', branch='master', path=''):
        repo_info = repolib.get(
            username=username, repo=repo, _type=_type,
            branch=branch, path=path
        )
        return jsonify(repo_info)


    @app.route('/git/api/<path:path>')
    def git_api(path):
        resp = session.get(f'/{path}')
        if resp.status_code == requests.codes.ok:
            return jsonify(resp.json())
        return abort(404)


    @app.after_request
    def after_request(response):
        return response

    return app


def add_jinja_globals(app):
    pass


def create_app():
    app = Flask(__name__, static_url_path='/static')
    app.debug = settings.DEBUG
    app.config.from_object(settings.config)
    webpack.init_app(app)
    add_routes(app)
    add_jinja_globals(app)
    return app

app = create_app()

if __name__ == '__main__':
    port = 5000
    conf = sys.argv[1:2] and sys.argv[1:2][0] or 'dev'
    app = create_app()
    if settings.DEBUG:
        app.env = 'development'
        app.run('127.0.0.1', port, debug=settings.DEBUG)
    else:
        http_server = WSGIServer(('', port), app)
        http_server.serve_forever()
