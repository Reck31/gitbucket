import requests

from flask import abort

from handlers.portal import session
from handlers.helpers import format_time


fields = [
    'public_repos', 'followers', 'following'
]
user_fields = [
    'avatar_url', 'email', 'name', 'location', 'bio',
    'login'
]
repo_fields = [
    'name', 'description', 'forks_count', 'full_name',
    'updated_at', 'stargazers_count', 'license', 'parent',
    'topics', 'language', 'id'
]


def get_repos(username):
    resp = session.get(f'/users/{username}/repos')
    if resp.status_code == requests.codes.ok:
        repos = resp.json()
        result = []
        for repo in repos:
            repo_info = dict((field, repo.get(field, None)) for field in repo_fields)
            repo_info['relative_update_time'] = format_time(repo_info['updated_at'])
            result.append(repo_info)
        return result


def get(username, include_repos=False):
    resp = session.get(f'/users/{username}')
    if resp.status_code == requests.codes.ok:
        data = resp.json()
        result = dict((field, data.get(field, None)) for field in fields)
        result['user'] = dict((field, data.get(field, None)) for field in user_fields)
        if include_repos:
            result['repos'] = get_repos(username)
        return result
