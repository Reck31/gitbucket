import requests
import base64

from flask import abort

from collections import OrderedDict
from handlers.portal import session
from handlers.helpers import format_time, humanize_file_size

import handlers.profile as profilelib


user_fields = [
    'avatar_url', 'email', 'name', 'location', 'bio',
    'login'
]

fields = [
    'name', 'full_name', 'owner', 'description',
    'language', 'default_branch', 'git_url', 'ssh_url',
    'clone_url', 'homepage', 'id'
]


def get_files(username, repo, branch, path):
    url = f'/repos/{username}/{repo}/contents'
    if path:
        url += f'/{path}'
    resp = session.get(url)
    if resp.status_code == requests.codes.ok:
        files = resp.json()
        return files
    return []


def decode_content_and_split(content):
    return base64.b64decode(content).decode('utf-8').splitlines()


def get_content(username, repo, _type, branch, path):
    url = f'/repos/{username}/{repo}/contents'
    if path:
        url += f'/{path}'
    json = {
        'ref': branch
    }
    resp = session.get(url, json=json)
    if resp.status_code == requests.codes.ok:
        data = resp.json()
        if _type == 'blob':
            content = decode_content_and_split(data['content'])
            content_meta = dict(
                lines=len(content),
                sloc=len(list(filter(None, content))),
                size=humanize_file_size(data['size'])
            )
            return decode_content_and_split(data['content']), content_meta
        else:
            return data, None


def get_commits(username, repo, branch, count=False):
    url = f'/repos/{username}/{repo}/commits'
    json = {
        'ref': branch
    }
    resp = session.get(url, json=json)
    if resp.status_code == requests.codes.ok:
        commits = resp.json()
        if count:
            return len(commits)
        result = OrderedDict()
        for commit in commits:
            date = format_time(commit['commit']['author']['date'])
            if date not in result:
                result[date] = [commit]
            else:
                result[date].append(commit)
        return result


def get(username, repo, _type='', branch='master', path=''):
    resp = session.get(f'/repos/{username}/{repo}')
    if resp.status_code == requests.codes.ok:
        data = resp.json()
        result = dict((field, data.get(field, None)) for field in fields)
        result['owner'].update(profilelib.get(username)['user'])
        if _type == 'commits':
            result['commits'] = get_commits(username, repo, branch)
        else:
            result['commits_count'] = get_commits(username, repo, branch, True)
            result['content'], result['content_meta'] = get_content(username, repo, _type, branch, path)
        result['type'] = _type or 'tree'
        return result
