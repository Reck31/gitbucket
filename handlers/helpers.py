from flask_webpack import Webpack
from datetime import datetime


class WebpackExt(Webpack):
    """
    Extend the Webpack plugin to normalize asset urls in production
    """

    def asset_url_for(self, asset):
        if '//' in asset:
            return asset

        print(asset, self.assets)

        if asset not in self.assets:
            return '/404'

        fil = self.assets[asset]
        if fil.startswith('./'):
            fil = fil[2:]

        return '{0}{1}'.format(self.assets_url, fil)

webpack = WebpackExt()

def format_time(timestamp):
    then = datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%SZ")
    now = datetime.now()
    delta = now - then
    if delta.days == 0:
        return 'today'
    if delta.days < 10:
        return f'{delta.days} days ago'
    if then.year == now.year:
        return 'on {}'.format(then.strftime('%b %d'))
    else:
        return 'on {}'.format(then.strftime('%b %d, %Y'))


def humanize_file_size(size):
    if size < 1000:
        return f'{size} Bytes'
    elif size < 10**6:
        size = size / 10**3
        return f'{size} KB'
    elif size < 10**9:
        size = size / 10**6
        return f'{size} MB'
    else:
        size = size / 10**9
        return f'{size} GB'
