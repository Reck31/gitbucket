import settings
import requests

from urllib.parse import urljoin
from requests import Session


class PrefixedURLSession(Session):
    def __init__(self, baseurl, *args, **kw):
        super(PrefixedURLSession, self).__init__(*args, **kw)
        self.baseurl = baseurl

    def request(self, method, url, *args, **kw):
        url += f'?client_id={settings.CLIENT_ID}&client_secret={settings.CLIENT_SECRET}'
        url = urljoin(self.baseurl, url)
        return super(PrefixedURLSession, self).request(method, url, *args, **kw)


session = PrefixedURLSession(settings.GITHUB_API_URL)
