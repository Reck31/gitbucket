import requests

from . import testdata
from handlers import repository
from handlers.portal import session
from handlers.helpers import format_time

from collections import OrderedDict


def test_get_files():
    resp_get_files_1 = repository.get_files(**testdata.test_get_files_1)
    resp = session.get(testdata.exp_get_files_url_1)
    if resp.status_code == requests.codes.ok:
        exp_get_files_1 = resp.json()
        assert resp_get_files_1 == exp_get_files_1

    resp_get_files_2 = repository.get_files(**testdata.test_get_files_2)
    assert resp_get_files_2 == testdata.exp_empty_list


def test_decode_content_and_split():
    resp_decode_content_1 = repository.decode_content_and_split(testdata.test_decode_content_1)
    resp_decode_content_2 = repository.decode_content_and_split(testdata.test_decode_content_2)
    assert resp_decode_content_1 == testdata.exp_decode_content_1
    assert resp_decode_content_2 == testdata.exp_decode_content_2


def test_get_content():
    resp_get_contents_tree, _ = repository.get_content(**testdata.test_get_content_tree)
    resp = session.get(testdata.exp_get_contents_tree_url)
    if resp.status_code == requests.codes.ok:
        exp_get_contents_tree = resp.json()
        assert resp_get_contents_tree == exp_get_contents_tree

    resp_get_contents_blob, _ = repository.get_content(**testdata.test_get_content_blob)
    resp = session.get(testdata.exp_get_contents_blob_url)
    if resp.status_code == requests.codes.ok:
        resp = resp.json()
        exp_get_contents_blob = repository.decode_content_and_split(resp['content'])
        assert resp_get_contents_blob == exp_get_contents_blob


def test_get_commits():
    resp_get_commits = repository.get_commits(**testdata.test_get_commits)
    resp = session.get(testdata.exp_get_commits_url)
    if resp.status_code == requests.codes.ok:
        commits = resp.json()
        exp_get_commits = OrderedDict()
        for commit in commits:
            date = format_time(commit['commit']['author']['date'])
            if date not in exp_get_commits:
                exp_get_commits[date] = [commit]
            else:
                exp_get_commits[date].append(commit)
    assert resp_get_commits == exp_get_commits
