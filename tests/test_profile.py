import requests
import settings

from . import testdata
from handlers.profile import repo_fields, get_repos
from handlers.portal import session
from handlers.helpers import format_time


def test_get_repos():
    resp_get_repos = get_repos(**testdata.test_get_repos)

    req_url = f'{settings.GITHUB_API_URL}/'
    resp = requests.get('http://api.github.com/' + testdata.exp_get_repos_url)
    if resp.status_code == requests.codes.ok:
        repos = resp.json()
        exp_get_repos = []
        for repo in repos:
            repo_info = dict((field, repo.get(field, None)) for field in repo_fields)
            repo_info['relative_update_time'] = format_time(repo_info['updated_at'])
            exp_get_repos.append(repo_info)
        assert resp_get_repos == exp_get_repos
