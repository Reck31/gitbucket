

test_time_1 = '2018-11-17T09:57:52Z'
exp_time_1 = '2 days ago'
test_time_2 = '2014-02-15T05:40:48Z'
exp_time_2 = 'on Feb 15, 2014'

test_size_1 = 88
exp_size_1 = '88 Bytes'
test_size_2 = 9912
exp_size_2 = '9.912 KB'


test_get_files_1 = dict(
    username='rakeshgunduka',
    repo='gaapi',
    branch='master',
    path=''
)
exp_get_files_url_1 = 'repos/rakeshgunduka/gaapi/contents'
test_get_files_2 = dict(
    username='rakeshgunduka',
    repo='Wumpus-World',
    branch='master',
    path='randompath'
)
exp_empty_list = []


test_decode_content_1 = 'YnVpbGQKZGlzdAoqLmVnZy1pbmZv\n'
exp_decode_content_1 = ['build', 'dist', '*.egg-info']

test_decode_content_2 = 'aW5jbHVkZSBSRUFETUUucnN0\n'
exp_decode_content_2 = ['include README.rst']


test_get_content_tree = dict(
    username='rakeshgunduka',
    repo='gaapi',
    _type='tree',
    branch='master',
    path='docs'
)
exp_get_contents_tree_url = 'repos/rakeshgunduka/gaapi/contents/docs'

test_get_content_blob = dict(
    username='rakeshgunduka',
    repo='gaapi',
    _type='blob',
    branch='master',
    path='docs/source/conf.py'
)
exp_get_contents_blob_url = 'repos/rakeshgunduka/gaapi/contents/docs/source/conf.py'

test_get_commits = dict(
    username='rakeshgunduka',
    repo='s3_dumps',
    branch='master'
)
exp_get_commits_url = 'repos/rakeshgunduka/s3_dumps/commits'

test_get_repos = dict(
    username='abcd'
)
exp_get_repos_url = 'users/abcd/repos'
