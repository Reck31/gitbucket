
from . import testdata
from handlers import helpers


def test_format_time():
    resp_time_1 = helpers.format_time(testdata.test_time_1)
    resp_time_2 = helpers.format_time(testdata.test_time_2)
    assert resp_time_1 == testdata.exp_time_1
    assert resp_time_2 == testdata.exp_time_2


def test_humanize_file_size():
    resp_size_1 = helpers.humanize_file_size(testdata.test_size_1) 
    resp_size_2 = helpers.humanize_file_size(testdata.test_size_2)
    assert resp_size_1 == testdata.exp_size_1
    assert resp_size_2 == testdata.exp_size_2
