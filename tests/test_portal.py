import settings
import requests
from handlers.portal import session



def test_portal_session():
    request_api_url = '/users/rakeshgunduka'
    portal_resp = session.get(request_api_url).json()

    req_url = f'{settings.GITHUB_API_URL}{request_api_url}'
    req_url += f'?client_id={settings.CLIENT_ID}&client_secret={settings.CLIENT_SECRET}'
    req_resp = requests.get(req_url).json()
    
    assert portal_resp == req_resp
