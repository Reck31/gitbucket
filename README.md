# GITHUB Mock

### 1. Install python 3
 *  `brew install python3`

### 2. Setting Cirtual Environment
  * `pip install virtualenv`
  * `pip install virtualenvwrapper` [more on virtualenv](https://virtualenvwrapper.readthedocs.io/en/latest/install.html)
  * run ```mkvirtualenv gitbucket --python=`which python3` ```

## 3. Clone repo
----------------
  * `git clone git@bitbucket.org:Reck31/gitbucket.git`
  * `cd gitbucket`

## 4. Install packages
----------------------
  * `pip install -r pip-requirements.txt`

## Run the server
  * run `python app.py`
  * Server will be running on 'http://localhost:5000'

## Run tests
```
nosetests -c tests/nose.cfg -xv
```
